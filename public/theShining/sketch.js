var jack = "All work and no play makes Jack a dull boy\n";
var count = 0;

function setup() {
  createCanvas(500,windowHeight);
  textAlign(LEFT,TOP);
  textSize(24);
  jack = jack.split("");  //split converts a textstring into an array of individual characters
}

function draw() {
  background(255);
  fill(0);
  var dullBoy = "";
  for(var i = 0; i< count;i++) {
    dullBoy= dullBoy+jack[i];
  }
  text(dullBoy,10,10,width-10,height-10);
}

function keyPressed() {
  //count counts the amount of times the keys have been pressed
  if(count < jack.length) {
    count++;
  } else {
    jack = jack.concat(jack);   //concat merges two arrays together. Here the jack array is extended to double size, so the sentence can be written more than once.
  }
}
