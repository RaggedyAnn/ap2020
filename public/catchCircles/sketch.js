var score = 0;

// array med ball-objekter
var balls = [];


function setup() {
  createCanvas(windowWidth,windowHeight);

  //for-loop, lægger 3 bolde ind i vores array
  for(var i = 0; i < 3; i++) {
    //var hej = width/3*i+20;
    balls[i]=new Ball();
  }
}

function draw() {
  background(255);

  //display score
  textSize(25);
  text(score,50,50);

  //for loop der udfører alle funktioner i vores klasse på ALLE objekter i vores array
  for(var j = 0; j < balls.length; j++) {
    balls[j].show();
    balls[j].move();
    //hvis rectanglet rammer bolden skal den starte igen fra venstre
    if(balls[j].x > width-150 && balls[j].x < width-100 && mouseY < balls[j].y && balls[j].y < mouseY+100) {
      //bolden der bliver ramt tages ud af arrayet
      balls.splice(j,1);
      score++;
      //en ny bold lægges på enden af arrayet
      balls.push(new Ball());
    }
  }

  //rectangel der følger musen
  fill(255,0,0);
  rect(width-150,mouseY,50,100);
}


class Ball {
  constructor() {
    this.size = 25;
    this.x = 0;
    this.y = random(height);
    this.radius = this.size/2;
    this.speedX = 4;
  }

  //funktion der indeholder alt det der har noget med det visuelle ved bolden at gøre
  show() {
    noStroke();
    fill(0);
    ellipse(this.x, this.y, this.size,this.size);

  }

  //funktionen der indeholder alt det der har noget med boldens bevægelsesmønster at gøre
  move() {
    this.x += this.speedX;

    if(this.x > width+this.size) {
      this.x = 0;
      this.y = random(height);
    }
  }

}
