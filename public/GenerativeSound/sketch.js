/*
Genrative code:
* Simple rules
* Repetition, but with variation
  * Let the algorithm take the wheel
* Emergence
  * Unpredictability
  * Even the simplest code when it emerges, the result becomes unimaginable
  * A logical outcome of the code that even the programmer did not foresee exactly
* Creating the unpredictable on a predictable machine
  * Existential randomness: Does your life follow an algorithm? When do you break
the algorithm?

Rules in this code:
* Read volume
* Read frequenzy
* Create colour
* Draw ellipse
* Move ellipse coordinates
  * If hitting edge: Change directions
* Repeat
*/

//sound setup
var mic;
var fft;

//antallet af bolde jeg vil lave
var ballNum = 3;

//var ballObject;

// array med ball-objekter
var balls = [];

function setup() {
  createCanvas(windowWidth,windowHeight);
  background(255);

  //sound setup
  mic = new p5.AudioIn();
  mic.start();
  fft = new p5.FFT();
  mic.connect(fft);

    //for-loop, lægger bolde ind i vores array
  for(var i = 0; i < ballNum; i++) {
    balls[i] = new Ball(width/ballNum*i+20);
  }

  //ballObject = new Ball();
}

function draw() {
  //gem farven baseret på lydinput i en variabel
  var sound = audio();

  //for loop der udfører alle funktioner i vores klasse på ALLE objekter i vores array
  for (var j = 0; j < balls.length; j++) {
    balls[j].show(sound);
    balls[j].move();
  }

  //ballObject.show();
  //ballObject.move();

}

//generates a colour based on the sound
function audio() {
  //get volume
  var vol = mic.getLevel();
  var mappedvol = map(vol, 0, 1, 0,255);

  //get frequenzy
  fft.analyze();
  var hz = fft.getCentroid();
  var mappedhz = map(hz, 1000,5000,0,255);

  return color(mappedhz,mappedvol,(mappedvol+mappedhz)/2);
}


class Ball {
  constructor(x) {
    this.size = 25;
    this.x = x;
    this.y = height/2;
    this.radius = this.size/2;
    this.speedX = 4;
    this.speedY = 4;
  }

  //funktion der indeholder alt det der har noget med det visuelle ved bolden at gøre
  show(col) {
    noStroke();
    fill(col);
    ellipse(this.x, this.y, this.size, this.size);
  }

  //funktionen der indeholder alt det der har noget med boldens bevægelsesmønster at gøre
  move() {
    this.x += this.speedX;
    this.y += this.speedY;

    if(this.x<this.radius || this.x>width-this.radius) {
      this.speedX *= -1;
    }

    if(this.y<this.radius || this.y>height-this.radius) {
      this.speedY *= -1
    }
  }
}
