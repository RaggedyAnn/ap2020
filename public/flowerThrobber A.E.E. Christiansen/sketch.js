/*
Heavily inspired by throbber made by Anne Elisabeth E Christiansen:
https://gitlab.com/anneellehauge/ap2020/-/tree/master/public/Miniex3
*/

var maxDiameter;
var theta;
var pedals = 5;


function setup() {
  createCanvas(600,500);
  maxDiameter = 30;
  theta = 0;

}

function draw() {
  background(180, 234, 245, 85);
  var diam = 170 + sin(theta) * maxDiameter ;
  translate(width/2,height/2);

  //pedals//
  push();
  for(var i = 0;i<pedals; i++) {
    push();
    strokeWeight(30);
    stroke(245, 219, 106);
    fill(245, 219, 106);
    //top pedal//
    rotate(radians(360/pedals)*i);
    ellipse(0,-60,50,diam);
    pop();
  }

  //center//
  noStroke();
  fill(120, 66, 18);
  circle(0,0,40);

  theta += .06; //speed of pulsing//
  pop();
}
