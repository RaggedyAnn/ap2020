//var myArray = [40,20,50,10];
var myArray = [];
function preload() {
  //the for-loop loads 3 pictures into the array. all images are named img0.png, img1.png,...
  for (var i = 0; i < 3; i++) {
    myArray[i]= loadImage("data/img" + i + ".png");
  }
}

function setup() {
  createCanvas(windowWidth,windowHeight);
  background(255);
  fill(0);
  //a for-loop that displays all elements in an array
   for (var i = 0; i < myArray.length; i++) {
      //if the array contains text or numbers
      //text(myArray[i], 20*i+20, height/2);
      //if the array contains images
      image(myArray[i], 100*i+100,height/4);
   }

  //a function that displays a random element in array that contains numbers or text
  //text(random(myArray), 20, height/2);
}

function draw() {

}
